import React,{useEffect} from 'react'
import { v4 as uuidv4 } from "uuid";


const Form = ({ input, setInput, todos, setTodos, editTodos, setEditTodos }) => {

    const onInputChange = (event) => {

        setInput(event.target.value);
    }

    const updateTodo = (tittle, id, completed) => {
        debugger
        const newTodo = todos.map((todo) => 
            todo.id === id ? {tittle, id, completed} : todo
        )
        setTodos(newTodo)
        setEditTodos("")
    }

    useEffect(()=>{
        if(editTodos){
            setInput(editTodos.tittle)
        }else{
            setInput("")
        }
    },[setInput,editTodos])

    const onFormSubmit = (event) => {
        event.preventDefault();
        if(!editTodos){
            setTodos([...todos, { id: uuidv4(), tittle: input, completed: false }])
            setInput("");
        }else{
            updateTodo(input, editTodos.id, editTodos.completed)
        }
      
    }

  
    return (
        <form onSubmit={onFormSubmit}>
            <input type="text" placeholder=" Enter a Todo..." className="task-input" value={input} onChange={onInputChange} requried />
            <button className="button-add" type="submit">{editTodos ? "OK":"Add"}</button>
        </form>
    )
}

export default Form
